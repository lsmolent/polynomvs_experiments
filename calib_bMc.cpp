#include <visp3/gui/vpDisplayGDI.h>
#include <visp3/core/vpImageConvert.h>
#include <visp3/gui/vpDisplayX.h>
#include <visp3/sensor/vpRealSense2.h>
#include <vector>
#include <string>
#include <visp3/core/vpImageDraw.h>
#include <visp3/core/vpMeterPixelConversion.h>
#include <visp3/io/vpImageIo.h>
#include <opencv2/core.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/ximgproc.hpp>
#include <visp3/detection/vpDetectorAprilTag.h>
#include <visp3/core/vpXmlParserCamera.h>
#include <visp/vpConfig.h>
#include <visp3/robot/vpRobotViper850.h>
#include <visp3/robot/vpRobotViper650.h>
#include <visp3/vision/vpHandEyeCalibration.h>
#if defined(VISP_HAVE_MODULE_IMGPROC)
#include <visp3/imgproc/vpImgproc.h>
#include <numeric>
#endif

#define MEASURES_TAKEN false
#define EST_BMC true
#define TEST_CTB false


int main() {

#if !MEASURES_TAKEN
    vpRobotViper650 robot;
    robot.setRobotState(vpRobot::STATE_POSITION_CONTROL);
#endif

    vpRealSense2 rs;
    rs2::config config;
    int width = 1280, height = 720, fps = 30;
    config.disable_stream(RS2_STREAM_DEPTH);
    config.enable_stream(RS2_STREAM_COLOR, width, height, RS2_FORMAT_RGBA8, fps);
    config.disable_stream(RS2_STREAM_INFRARED);

    rs.open(config);
    vpImage<vpRGBa> Ic(rs.getIntrinsics(RS2_STREAM_COLOR).height, rs.getIntrinsics(RS2_STREAM_COLOR).width);
    vpImage<uint16_t> depth_raw(rs.getIntrinsics(RS2_STREAM_COLOR).height, rs.getIntrinsics(RS2_STREAM_COLOR).width);
//    vpImage<vpRGBa> depth_color(static_cast<unsigned int>(height), static_cast<unsigned int>(width));
    vpImage<unsigned char> Ig(rs.getIntrinsics(RS2_STREAM_COLOR).height, rs.getIntrinsics(RS2_STREAM_COLOR).width);


    vpCameraParameters cam;
    cam = rs.getCameraParameters(RS2_STREAM_COLOR, vpCameraParameters::perspectiveProjWithDistortion);

#if !MEASURES_TAKEN
    vpDetectorAprilTag::vpAprilTagFamily tagFamily = vpDetectorAprilTag::TAG_36h11;
    vpDetectorAprilTag::vpPoseEstimationMethod poseEstimationMethod = vpDetectorAprilTag::HOMOGRAPHY_VIRTUAL_VS;
    double tagSize = 0.1605;
//    double tagSize = 0.0715;
    float quad_decimate = 1.0;
    int nThreads = 12;
    bool display_tag = false;
#endif

    int color_id = -1;
    unsigned int thickness = 2;
    bool align_frame = true;


#if !(defined(VISP_HAVE_X11) || defined(VISP_HAVE_GDI) || defined(VISP_HAVE_OPENCV))
    bool display_off = true;
    std::cout << "Warning: There is no 3rd party (X11, GDI or openCV) to dislay images..." << std::endl;
#else
    bool display_off = false;
#endif

#ifdef VISP_HAVE_X11
    vpDisplayX dc(Ic, 0, 0, "Color");
//    vpDisplayX dd( depth_color, 0, 700, "Depth");

#elif defined(VISP_HAVE_GDI)
    vpDisplayGDI dc(Ic, 0, 0, "Color");
    vpDisplayGDI di1(Ii1, 100, 100, "Infrared 1");
    vpDisplayGDI di2(Ii2, 100, 100, "Infrared 2");
#endif

#if !MEASURES_TAKEN
    vpDetectorAprilTag detector(tagFamily);
    detector.setAprilTagQuadDecimate(quad_decimate);
    detector.setAprilTagPoseEstimationMethod(poseEstimationMethod);
    detector.setAprilTagNbThreads(nThreads);
    detector.setDisplayTag(display_tag, color_id < 0 ? vpColor::none : vpColor::getColor(color_id), thickness);
    detector.setZAlignedWithCameraAxis(align_frame);

    std::vector<vpHomogeneousMatrix> cMo_detect(1);
    vpHomogeneousMatrix bMe;
    vpColVector ev(6),q(6);
    ev = 0;
    robot.getPosition(vpRobot::JOINT_STATE,q);
    std::vector<vpColVector> q_poses_calib(11);

    for(int i = 0; i < q_poses_calib.size(); ++i){
        vpColVector::loadYAML("/udd/lsmolent/Desktop/q" + std::to_string(i), q_poses_calib[i]);
        std::cout << "pose " << i << " " << q_poses_calib[i].t() << std::endl;
    }

//    q_poses_calib[0] = {vpMath::rad(-9), vpMath::rad(-88),vpMath::rad(144),vpMath::rad(8),vpMath::rad(109),vpMath::rad(-62)};
//    q_poses_calib[1] = {vpMath::rad(-38), vpMath::rad(-64),vpMath::rad(144),vpMath::rad(8),vpMath::rad(109),vpMath::rad(-107)};
//    q_poses_calib[2] = {vpMath::rad(-38), vpMath::rad(-50),vpMath::rad(144),vpMath::rad(50),vpMath::rad(79),vpMath::rad(-107)};
//    q_poses_calib[3] = {vpMath::rad(-65), vpMath::rad(-50),vpMath::rad(157),vpMath::rad(-22),vpMath::rad(76),vpMath::rad(-107)};
////    q_poses_calib[4] = {vpMath::rad(-65), vpMath::rad(-97),vpMath::rad(200),vpMath::rad(-3),vpMath::rad(65),vpMath::rad(-166)};
//    q_poses_calib[4] = {vpMath::rad(-32), vpMath::rad(-40),vpMath::rad(128),vpMath::rad(1),vpMath::rad(19),vpMath::rad(-17)};
////    q_poses_calib[5] = {vpMath::rad(-68), vpMath::rad(-44),vpMath::rad(134),vpMath::rad(-67),vpMath::rad(80),vpMath::rad(-184)};
////    q_poses_calib[7] = {vpMath::rad(-39), vpMath::rad(-40),vpMath::rad(139),vpMath::rad(55),vpMath::rad(-18),vpMath::rad(-42)};
////    q_poses_calib[5] = {vpMath::rad(-53), vpMath::rad(-98),vpMath::rad(202),vpMath::rad(-88),vpMath::rad(-36),vpMath::rad(19)};
////    q_poses_calib[7] = {vpMath::rad(10), vpMath::rad(-80),vpMath::rad(161),vpMath::rad(12),vpMath::rad(91),vpMath::rad(-60)};
//    q_poses_calib[5] = {vpMath::rad(32), vpMath::rad(-66),vpMath::rad(109),vpMath::rad(-55),vpMath::rad(80),vpMath::rad(20)};
#endif

    rs2::align align_to(RS2_STREAM_COLOR);
    int points = 0;

#if !MEASURES_TAKEN
    // for n positions
    for (auto &i : q_poses_calib) {
        robot.setPosition(vpRobot::JOINT_STATE, i);
        vpTime::wait(1000);
        rs.acquire(reinterpret_cast<unsigned char *>(Ic.bitmap),
                   NULL,
                   NULL, NULL, &align_to);
        vpImageConvert::convert(Ic, Ig);
//            vpDisplay::display(Ic);

        if (vpDisplay::getClick(Ic, true)) {
            bool detected = detector.detect(Ig, tagSize, cam, cMo_detect);
            if (detected) {
                vpDisplay::display(Ic);
                vpDisplay::displayFrame(Ic, cMo_detect[0], cam, tagSize / 2, vpColor::none, 3);
                ++points;
                vpHomogeneousMatrix::saveYAML("calib/cMo" + std::to_string(points) + ".yml", cMo_detect.at(0));
                robot.getPosition(vpRobot::JOINT_STATE, q);
                robot.get_fMe(q, bMe);
                vpHomogeneousMatrix::saveYAML("calib/bMe" + std::to_string(points) + ".yml", bMe);
                std::cout << "point No " << points << " saved" << std::endl;
                vpDisplay::flush(Ic);
            }
        }
        if (points == q_poses_calib.size()) {
            std::cout << q_poses_calib.size() <<  " measures taken" << std::endl;
            robot.setRobotState(vpRobot::STATE_STOP);
            break;
        }
        vpDisplay::flush(Ic);
    }

#endif
#if MEASURES_TAKEN
    points = 6;
#endif
#if EST_BMC
    std::cout << "No of points: " << points << std::endl;

    std::vector<vpHomogeneousMatrix> wMe(points);
    std::vector<vpHomogeneousMatrix> oMc(points);
    vpHomogeneousMatrix bMc;
    vpHomogeneousMatrix eMo;

    for (unsigned int i = 1; i <= points; i++) {
        std::ostringstream ss_fMe, ss_cMo;
        ss_fMe << "calib/bMe" << i << ".yml";
        ss_cMo << "calib/cMo" << i << ".yml";
        std::cout << "Use fMe=" << ss_fMe.str() << ", cMo=" << ss_cMo.str() << std::endl;

        vpHomogeneousMatrix wMe_;

        if (!vpHomogeneousMatrix::loadYAML(ss_fMe.str(), wMe_)) {
            std::cout << "Unable to read data from: " << ss_fMe.str() << std::endl;
            return EXIT_FAILURE;
        }
        wMe[i-1] = wMe_;

        vpHomogeneousMatrix cMo_;
        if (!vpHomogeneousMatrix::loadYAML(ss_cMo.str(), cMo_)) {
            std::cout << "Unable to read data from: " << ss_cMo.str() << std::endl;
            return EXIT_FAILURE;
        }
        oMc[i-1] = cMo_.inverse();
    }

    int ret = vpHandEyeCalibration::calibrate(oMc, wMe, eMo);

    if (ret == 0) {
        std::cout << std::endl << "** eye-to-hand calibration succeed" << std::endl;
        std::cout << std::endl << "** eye-in-hand (eMo) transformation estimated:" << std::endl;
        std::cout << eMo << std::endl;
        vpHomogeneousMatrix::saveYAML("calib/eMo.yml", eMo);
//        bMc = vpHomogeneousMatrix::mean(wMe) * eMo * vpHomogeneousMatrix::mean(oMc);

        std::vector<vpHomogeneousMatrix> sum(points);
        for(int i = 0; i < points; ++i){
            sum[i] = (wMe[i]*eMo*oMc[i]);
        }
        bMc = vpHomogeneousMatrix::mean(sum);
        std::cout << std::endl << "** eye-to-hand (bMc) transformation estimated:" << std::endl;
        std::cout << bMc << std::endl;
        std::cout << std::endl << "** eye-to-hand (cMb) transformation is:" << std::endl;
        std::cout << bMc.inverse() << std::endl;
        vpHomogeneousMatrix::saveYAML("calib/bMc.yml", bMc);
    }
    else {
        std::cout << std::endl << "** eye-to-hand calibration failed" << std::endl;
    }
#endif

#if TEST_CTB && MEASURES_TAKEN
    vpHomogeneousMatrix bMc, eMo;
    vpHomogeneousMatrix::loadYAML("calib/bMc.yml", bMc);
    vpHomogeneousMatrix::loadYAML("calib/eMo.yml", eMo);
    vpRobotViper650 robot;
    robot.setMaxTranslationVelocity(0.05);
    robot.setMaxRotationVelocity(0.05);
    robot.setRobotState(vpRobot::STATE_POSITION_CONTROL);
    robot.setPosition("/udd/lsmolent/Desktop/poly_exp_init_pose.yml.pos");
    vpColVector q,cv(6,0);
    cv[1] = 0.04; cv[5] = 0.;
    vpHomogeneousMatrix bMe;
    vpMatrix eJe;
    vpVelocityTwistMatrix cVe;
//    robot.setRobotState(vpRobot::STATE_POSITION_CONTROL);
//    robot.setPosition(vpRobot::JOINT_STATE, q_poses_calib[2]);
    robot.getPosition(vpRobot::JOINT_STATE, q);
    robot.get_fMe(q, bMe);
    robot.get_eJe(eJe);
//    robot.init(vpViper650::, bMe.inverse()*bMc);

    cVe.buildFrom(bMc.inverse() * bMe);
    robot.setRobotState(vpRobot::STATE_VELOCITY_CONTROL);
    while(true){

        rs.acquire(reinterpret_cast<unsigned char *>(Ic.bitmap),
                   NULL,
                   NULL, NULL, &align_to);

        vpDisplay::display(Ic);
        vpDisplay::displayFrame(Ic, bMc.inverse()*bMe*eMo, cam, 0.3, vpColor::none, 2);
        vpDisplay::displayFrame(Ic, bMc.inverse(),cam, 0.3, vpColor::none, 2);

        robot.getPosition(vpRobot::JOINT_STATE, q);
        robot.get_fMe(q, bMe);
        robot.get_eJe(eJe);
        cVe.buildFrom(bMc.inverse() * bMe);
        robot.setVelocity(vpRobot::ARTICULAR_FRAME, (cVe*eJe).inverseByLU()*cv);

        vpDisplay::flush(Ic);

        if (vpDisplay::getClick(Ic, false))
            break;
    }
    robot.setRobotState(vpRobot::STATE_STOP);
#endif
    rs.close();

    return 0;
}
