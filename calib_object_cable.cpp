//
// Created by lsmolent on 11/18/21.
//

#include <visp3/gui/vpDisplayGDI.h>
#include <visp3/core/vpImageConvert.h>
#include <visp3/gui/vpDisplayX.h>
#include <visp3/sensor/vpRealSense2.h>
#include <vector>
#include <string>
#include <visp3/core/vpImageDraw.h>
#include <visp3/core/vpMeterPixelConversion.h>
#include <visp3/core/vpPixelMeterConversion.h>
#include <visp3/detection/vpDetectorAprilTag.h>
#include <visp3/core/vpXmlParserCamera.h>
#include <visp/vpConfig.h>
#include <visp3/robot/vpRobotViper650.h>
#include <visp3/vision/vpHandEyeCalibration.h>
#include <visp3/core/vpThread.h>
#if defined(VISP_HAVE_MODULE_IMGPROC)
#include <visp3/imgproc/vpImgproc.h>
#endif


/*void camera_flow(vpRealSense2& rs, vpImage<vpRGBa>& Ic, vpImage<uint16_t>& depth_raw, const rs2::align* align_to){
    while(true){
        rs.acquire(reinterpret_cast<unsigned char *>(Ic.bitmap),
                   reinterpret_cast<unsigned char *>(depth_raw.bitmap),
                   NULL, NULL, &align_to);
        vpImageConvert::createDepthHistogram(depth_raw, depth_color);
        vpImageConvert::convert(Ic, Ig);
        vpMouseButton::vpMouseButtonType button;

        vpDisplay::display(Ic);
        vpDisplay::display(depth_color);
        if (vpDisplay::getClick(Ic, false))
            break;
    }
}*/

int main() {
    vpRealSense2 rs;
    rs2::config config;
    int width = 1280, height = 720, fps = 30;
    config.enable_stream(RS2_STREAM_DEPTH, width, height, RS2_FORMAT_Z16, fps);
    config.enable_stream(RS2_STREAM_COLOR, width, height, RS2_FORMAT_RGBA8, fps);
    config.disable_stream(RS2_STREAM_INFRARED);

    rs.open(config);
    vpImage<vpRGBa> Ic(rs.getIntrinsics(RS2_STREAM_COLOR).height, rs.getIntrinsics(RS2_STREAM_COLOR).width);
    vpImage<uint16_t> depth_raw(rs.getIntrinsics(RS2_STREAM_COLOR).height, rs.getIntrinsics(RS2_STREAM_COLOR).width);
    vpImage<vpRGBa> depth_color(static_cast<unsigned int>(height), static_cast<unsigned int>(width));
    vpImage<unsigned char> Ig(rs.getIntrinsics(RS2_STREAM_COLOR).height, rs.getIntrinsics(RS2_STREAM_COLOR).width);

    rs2::align align_to(RS2_STREAM_COLOR);
    const float depth_scale = rs.getDepthScale();

    vpCameraParameters cam;
    cam = rs.getCameraParameters(RS2_STREAM_COLOR, vpCameraParameters::perspectiveProjWithDistortion);

    vpDetectorAprilTag::vpAprilTagFamily tagFamily = vpDetectorAprilTag::TAG_36h11;
    vpDetectorAprilTag::vpPoseEstimationMethod poseEstimationMethod = vpDetectorAprilTag::HOMOGRAPHY_VIRTUAL_VS;
    double tagSize = 0.08996;
    float quad_decimate = 1.0;
    int nThreads = 12;
    bool display_tag = false;
    int color_id = -1;
    unsigned int thickness = 2;
    bool align_frame = true;
    vpDisplayX dc(Ic, 0, 0, "Color");
    vpDisplayX dd(depth_color, 0, 700, "Depth");

    vpDetectorAprilTag detector(tagFamily);
    detector.setAprilTagQuadDecimate(quad_decimate);
    detector.setAprilTagPoseEstimationMethod(poseEstimationMethod);
    detector.setAprilTagNbThreads(nThreads);
    detector.setDisplayTag(display_tag, color_id < 0 ? vpColor::none : vpColor::getColor(color_id), thickness);
    detector.setZAlignedWithCameraAxis(align_frame);

    std::vector<vpHomogeneousMatrix> cMo_detect(1);
    vpHomogeneousMatrix bMc, bMe, eMo;
    vpHomogeneousMatrix::loadYAML("calib/bMc.yml", bMc);
    vpHomogeneousMatrix::loadYAML("calib/eMo.yml", eMo);
    vpImagePoint ip;
    vpColVector cp2(4), op2(4), cp1(4);

    vpColVector q(6);
    vpRobotViper650 robot;
    robot.setRobotState(vpRobot::STATE_POSITION_CONTROL);
    robot.setPosition("/udd/lsmolent/Desktop/poly_exp_init_pose.yml.pos");
    robot.getPosition(vpRobot::JOINT_STATE, q);
    robot.get_fMe(q, bMe);
    robot.setRobotState(vpRobot::STATE_STOP);

    vpMouseButton::vpMouseButtonType button;

    vpDisplay::display(Ic);
    vpDisplay::display(depth_color);

    bool point_taken = false;
    double x,y, i,j;
    int p_number = 2;
    while (true) {
        rs.acquire(reinterpret_cast<unsigned char *>(Ic.bitmap),
                   reinterpret_cast<unsigned char *>(depth_raw.bitmap),
                   NULL, NULL, &align_to);
        vpImageConvert::createDepthHistogram(depth_raw, depth_color);
        vpImageConvert::convert(Ic, Ig);

        vpDisplay::display(Ic);
        vpDisplay::display(depth_color);

        if(vpDisplay::getClickUp(Ic, ip, button, false)) {
            vpPixelMeterConversion::convertPoint(cam, ip, x, y);
            i = ip.get_i();
            j = ip.get_j();
            point_taken = true;
        }
        if(point_taken){
            point_taken = false;
            std::cout << "i=" << i << " j=" << j << std::endl;
            vpImageDraw::drawPoint(Ic, ip, vpColor::darkRed, 3);
            vpDisplay::display(Ic);
            vpDisplay::flush(Ic);
            double sum = 0;
            for (int ii = 0; ii < 100; ++ii) {
                rs.acquire(NULL, reinterpret_cast<unsigned char *>(depth_raw.bitmap),
                           NULL, NULL, &align_to);
                sum += (double) depth_raw[(int) i][(int) j] * depth_scale;
            }
            auto Z = sum / 100;
            vpColVector cp(4);
            cp[0] = x * Z;
            cp[1] = y * Z;
            cp[2] = Z;
            cp[3] = 1;
            vpColVector::saveYAML("calib/cp" + std::to_string(p_number) + ".yml", cp);
            p_number -= 1;
            if(p_number == 0) break;
        }

        vpDisplay::flush(Ic);
        vpDisplay::flush(depth_color);

        if (vpDisplay::getClick(depth_color, false))
            break;
    }

    auto cMo2 = bMc.inverse() * bMe * eMo;
    std::cout << "new cMo=\n" << cMo2 << "\n";
    vpColVector::loadYAML("calib/cp2.yml",cp2);
    op2 = cMo2.inverse() * cp2;
    std::cout << "op2 estimated = " << op2.t() << std::endl;
    vpColVector::saveYAML("calib/op2_estimated.yml", op2);

    return 0;
}